package com.personinfo.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.personinfo.model.Person;

public interface PersonRepo extends JpaRepository<Person, Serializable> {

}
