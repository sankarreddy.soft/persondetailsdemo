package com.personinfo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.personinfo.model.Person;
import com.personinfo.repository.PersonRepo;


@Controller
public class PersonController {
	@Autowired
	PersonRepo personRepo;
	
	@RequestMapping("/personinfo")
	public String home() {
		
		return "personinfo.jsp";
		
	}
	
	
	 
	@RequestMapping("/addPerson") 
	public String addPerson(Person person) {
	 personRepo.save(person); 
	 return "Successpage.jsp"; }
	
	
	
	@RequestMapping("/getPersondata")
	public ModelAndView getPersondata() {
		List<Person> person = personRepo.findAll();
		ModelAndView mv = new ModelAndView("ShowPersron.jsp");
		mv.addObject(person);
		return mv;
	}
	@RequestMapping("/deletePersonbyId")
	public String getPersondata(@RequestParam int personid) {
		personRepo.deleteById(personid);
		return "personinfo.jsp";
	}
	 

}
